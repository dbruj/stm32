/*
 * itoa.c
 *
 *  Created on: 4 sie 2014
 *      Author: Dariusz
 */

#include "itoa.h"

char* itoa(uint32_t val, uint16_t base)
{

    static char buf[32] = {0};
    if(val==0){

        return "0";
    }
    int i = 30;

    for(; val && i ; --i, val /= base)

        buf[i] = "0123456789ABCDEF"[val % base];

    return &buf[i+1];
}


