/*
 * circular_buffer.c
 *
 *  Created on: 3 cze 2014
 *      Author: dbruj
 */

#include <stdint.h>
#include <stdlib.h>

#include "circular_buffer.h"

void misc_circularbuffer_init(CircularBuffer *cb, uint16_t size) {
    cb->size  = size;
    cb->start = 0;
    cb->end   = 0;
    cb->s_msb = 0;
    cb->e_msb = 0;
    cb->elems = (char *)calloc(cb->size, sizeof(char));
}

int misc_circularbuffer_isFull(CircularBuffer *cb) {
    return cb->end == cb->start && cb->e_msb != cb->s_msb; }

int misc_circularbuffer_isEmpty(CircularBuffer *cb) {
    return cb->end == cb->start && cb->e_msb == cb->s_msb; }

void misc_circularbuffer_incr(CircularBuffer *cb, uint16_t *p, uint16_t *msb) {
    *p = *p + 1;
    if (*p == cb->size) {
        *msb ^= 1;
        *p = 0;
    }
}

void misc_circularbuffer_write(CircularBuffer *cb, char *elem) {
    cb->elems[cb->end] = *elem;
    if (misc_circularbuffer_isFull(cb)) /* full, overwrite moves start pointer */
        misc_circularbuffer_incr(cb, &cb->start, &cb->s_msb);
    misc_circularbuffer_incr(cb, &cb->end, &cb->e_msb);
}

void misc_circularbuffer_read(CircularBuffer *cb, char *elem) {
    *elem = cb->elems[cb->start];
    misc_circularbuffer_incr(cb, &cb->start, &cb->s_msb);
}



