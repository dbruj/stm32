/*
 * itoa.h
 *
 *  Created on: 4 sie 2014
 *      Author: Dariusz
 */

#ifndef ITOA_H_
#define ITOA_H_

#include <stdint.h>

char* itoa(uint32_t val, uint16_t base);

#endif /* ITOA_H_ */
