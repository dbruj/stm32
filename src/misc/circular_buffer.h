/*
 * circular_buffer.h
 *
 *  Created on: 2 cze 2014
 *      Author: dbruj
 */

#ifndef MISC_CIRCULAR_BUFFER_H_
#define MISC_CIRCULAR_BUFFER_H_

/* This approach adds one bit to end and start pointers */

/**
 *  Circular buffer object
 */
typedef struct {
    uint16_t        size;   /* maximum number of elements           */
    uint16_t        start;  /* index of oldest element              */
    uint16_t        end;    /* index at which to write new element  */
    uint16_t        s_msb;
    uint16_t        e_msb;
    char*	   		elems;  /* vector of elements                   */
} CircularBuffer;

/**
 * Initialize circular buffer.
 */
void misc_circularbuffer_init(CircularBuffer *cb, uint16_t size);

/**
 * Is circular buffer full?
 */
int misc_circularbuffer_isFull(CircularBuffer *cb);

/**
 * Is circular buffer empty?
 */
int misc_circularbuffer_isEmpty(CircularBuffer *cb);

/**
 * Increment circular buffer.
 */
void misc_circularbuffer_incr(CircularBuffer *cb, uint16_t *p, uint16_t *msb);

/**
 * Write into circular buffer.
 */
void misc_circularbuffer_write(CircularBuffer *cb, char *elem);

/**
 * Read from circular buffer.
 */
void misc_circularbuffer_read(CircularBuffer *cb, char *elem);

#endif /* MISC_CIRCULAR_BUFFER_H_ */
