
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "stm32f10x.h"

#include "stm32/rcc.h"
#include "stm32/gpio.h"
#include "stm32/usart.h"
#include "stm32/rtc.h"

#include "misc/itoa.h"
#include "misc/circular_buffer.h"

#include "parser/parser.h"

int main(void)
{
	// Initialize system.
	stm32_rcc_init();
	stm32_gpio_init();
	stm32_usart_init(USART2, 9600);
	stm32_rtc_init();

	//char* data = "test2";

	//stm32_usart_init(USART1, 115200);
	//stm32_usart_setString(USART1, data);

	uint32_t lastClockTick = 0;

	// Infinite loop
	while (1)
	{


		uint32_t epoch = stm32_rtc_get();
		if (lastClockTick < epoch)
		{
			lastClockTick = epoch;

			char buff[255] = {'(', 0};

			char* buf = itoa(epoch, 10);
			strcpy((char*)&buff+1, buf);
			strcpy((buff+strlen(buf)+1), " % 2 ) > 0");

			double result = 0;
			parser_init();
			parser_parse(&result, buff);

			stm32_usart_setString(USART2, buff);
			if (result > 0)
			{
				stm32_usart_setString(USART2, " - OK\n\r");
			} else {
				stm32_usart_setString(USART2, " - FALSE\n\r");
			}
		}
	}

	return 0;
}

