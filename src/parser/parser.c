

#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <ctype.h>

#include "parser.h"
#include "constants.h"
#include "functions.h"


/// PARSER DATA
char expr[EXPR_LEN_MAX+1];    // holds the expression

char token[NAME_LEN_MAX+1];   // holds the token
TOKENTYPE token_type;         // type of the token

char* e;                      // points to a character in expr

/*
 * Initializes all data with zeros and empty strings
 */
void parser_init()
{
    expr[0] = '\0';
    e = NULL;

    token[0] = '\0';
    token_type = NOTHING;
}

/**
 * parses and evaluates the given expression
 * On error, an error is set.
 */
uint32_t parser_parse(double* result, const char new_expr[])
{
    // check the length of expr
    if ((int)strlen(new_expr) > EXPR_LEN_MAX)
    {
        return 200;
    }

    // initialize all variables
    strncpy(expr, new_expr, EXPR_LEN_MAX - 1);  // copy the given expression to expr
    e = expr;                                  // let e point to the start of the expression
    double ans = 0;

    parser_getToken();
    if (token_type == DELIMETER && *token == '\0')
    {
        return 4;
    }

    uint32_t err = parser_parse_level1(&ans);

    // check for garbage at the end of the expression
    // an expression ends with a character '\0' and token_type = delimeter
    if (token_type != DELIMETER || *token != '\0')
    {
        if (token_type == DELIMETER)
        {
            // user entered a not existing operator like "//"
            return 101;
        }
        else
        {
            return 5;
        }
    }

    // Exit parser.
    *result = ans;
    return err;
}

/*
 * checks if the given char c is a minus
 */
uint8_t isMinus(const char c)
{
    if (c == 0) return 0;
    return c == '-';
}

/*
 * checks if the given char c is whitespace
 * whitespace when space chr(32) or tab chr(9)
 */
uint8_t isWhiteSpace(const char c)
{
    if (c == 0) return 0;
    return c == 32 || c == 9;  // space or tab
}

/*
 * checks if the given char c is a delimeter
 * minus is checked apart, can be unary minus
 */
uint8_t isDelimeter(const char c)
{
    if (c == 0) return 0;
    return strchr("&|<>=+/*%^!", c) != 0;
}

/*
 * checks if the given char c is NO delimeter
 */
uint8_t isNotDelimeter(const char c)
{
    if (c == 0) return 0;
    return strchr("&|<>=+-/*%^!()", c) != 0;
}

/*
 * checks if the given char c is a letter or undersquare
 */
uint8_t isAlpha(const char c)
{
    if (c == 0) return 0;
    return strchr("ABCDEFGHIJKLMNOPQRSTUVWXYZ_", toupper((uint8_t)c)) != 0;
}

/*
 * checks if the given char c is a digit or dot
 */
uint8_t isDigitDot(const char c)
{
    if (c == 0) return 0;
    return strchr("0123456789.", c) != 0;
}

/*
 * checks if the given char c is a digit
 */
uint8_t isDigit(const char c)
{
    if (c == 0) return 0;
    return strchr("0123456789", c) != 0;
}


/**
 * Get next token in the current string expr.
 * Uses the Parser data expr, e, token, t, token_type and err
 */

uint32_t parser_getToken()
{
    token_type = NOTHING;
    char* t;           // points to a character in token
    t = token;         // let t point to the first character in token
    *t = '\0';         // set token empty

    // skip over whitespaces
    while (*e == ' ' || *e == '\t')     // space or tab
    {
        e++;
    }

    // check for end of expression
    if (*e == '\0')
    {
        // token is still empty
        token_type = DELIMETER;
        return 0;
    }

    // check for minus
    if (*e == '-')
    {
        token_type = DELIMETER;
        *t = *e;
        e++;
        t++;
        *t = '\0';  // add a null character at the end of token
        return 0;
    }

    // check for parentheses
    if (*e == '(' || *e == ')')
    {
        token_type = DELIMETER;
        *t = *e;
        e++;
        t++;
        *t = '\0';
        return 0;
    }

    // check for operators (delimeters)
    if (isDelimeter(*e))
    {
        token_type = DELIMETER;
        while (isDelimeter(*e))
        {
            *t = *e;
            e++;
            t++;
        }
        *t = '\0';  // add a null character at the end of token
        return 0;
    }

    // check for a value
    if (isDigitDot(*e))
    {
        token_type = NUMBER;
        while (isDigitDot(*e))
        {
            *t = *e;
            e++;
            t++;
        }

        // check for scientific notation like "2.3e-4" or "1.23e50"
        if (toupper((uint8_t)*e) == 'E')
        {
            *t = *e;
            e++;
            t++;

            if (*e == '+' || *e == '-')
            {
                *t = *e;
                e++;
                t++;
            }

            while (isDigit(*e))
            {
                *t = *e;
                e++;
                t++;
            }
        }

        *t = '\0';
        return 0;
    }

    // check for variables or functions
    if (isAlpha(*e))
    {
        while (isAlpha(*e) || isDigit(*e))
        //while (isNotDelimeter(*e))
        {
            *t = *e;
            e++;
            t++;
        }
        *t = '\0';  // add a null character at the end of token

        // check if this is a variable or a function.
        // a function has a parentesis '(' open after the name
        char* e2 = NULL;
        e2 = e;

        // skip whitespaces
        while (*e2 == ' ' || *e2 == '\t')     // space or tab
        {
            e2++;
        }

        if (*e2 == '(')
        {
            token_type = FUNCTION;
        }
        else
        {
            token_type = VARIABLE;
        }
        return 0;
    }

    // something unknown is found, wrong characters -> a syntax error
    token_type = UNKNOWN;
    while (*e != '\0')
    {
        *t = *e;
        e++;
        t++;
    }
    *t = '\0';
    return 1;
}

/*
 * assignment of variable or function
 */
uint32_t parser_parse_level1(double* result)
{
    if (token_type == VARIABLE)
    {
        // copy current token
        char* e_now = e;
        TOKENTYPE token_type_now = token_type;
        char token_now[NAME_LEN_MAX+1];
        strcpy(token_now, token);

        parser_getToken();
        if (strcmp(token, "=") == 0)
        {
            // assignment
            double ans;
            parser_getToken();

            uint32_t err = parser_parse_level2(&ans);

            //return ans;
            // Przepisz wynik do result, a blad zwroc jako wynik funkcji.
            *result = ans;
            return err;
        }
        else
        {
            // go back to previous token
            e = e_now;
            token_type = token_type_now;
            strcpy(token, token_now);
        }
    }

    return parser_parse_level2(result);
}

uint32_t parser_parse_level2(double* result)
{
    uint32_t err = 0;

    int op_id;
    double ans = 0;
    err = parser_parse_level3(&ans);

    op_id = parser_get_operator_id(token);
    while (op_id == AND || op_id == OR || op_id == BITSHIFTLEFT || op_id == BITSHIFTRIGHT)
    {
        parser_getToken();

        double ansl3 = 0;
        err = parser_parse_level3(&ansl3);
        err = parser_eval_operator(op_id, &ans, &ansl3, &ans);
        op_id = parser_get_operator_id(token);
    }

    *result = ans;
    return err;
}

/*
 * conditional operators
 */
uint32_t parser_parse_level3(double* result)
{
    uint32_t err = 0;

    int op_id;
    double ans = 0;
    err = parser_parse_level4(&ans);

    op_id = parser_get_operator_id(token);
    while (op_id == EQUAL || op_id == UNEQUAL || op_id == SMALLER || op_id == LARGER || op_id == SMALLEREQ || op_id == LARGEREQ)
    {
        parser_getToken();

        double ansl4 = 0;
        err = parser_parse_level4(&ansl4);
        err = parser_eval_operator(op_id, &ans, &ansl4, &ans);
        op_id = parser_get_operator_id(token);
    }

    *result = ans;
    return err;
}

/*
 * add or subtract
 */
uint32_t parser_parse_level4(double* result)
{
    uint32_t err = 0;

    int op_id;
    double ans = 0;
    err = parser_parse_level5(&ans);

    op_id = parser_get_operator_id(token);
    while (op_id == PLUS || op_id == MINUS)
    {
        parser_getToken();

        double ansl5 = 0;
        parser_parse_level5(&ansl5);
        err = parser_eval_operator(op_id, &ans, &ansl5, &ans);
        op_id = parser_get_operator_id(token);
    }

    *result = ans;
    return err;
}


/*
 * multiply, divide, modulus, xor
 */
uint32_t parser_parse_level5(double* result)
{
    uint32_t err = 0;

    int op_id;
    double ans = 0;
    err = parser_parse_level6(&ans);

    op_id = parser_get_operator_id(token);
    while (op_id == MULTIPLY || op_id == DIVIDE || op_id == MODULUS || op_id == XOR)
    {
        parser_getToken();

        double ansl6 = 0;
        parser_parse_level6(&ansl6);
        err = parser_eval_operator(op_id, &ans, &ansl6, &ans);
        op_id = parser_get_operator_id(token);
    }

    *result = ans;
    return err;
}


/*
 * power
 */
uint32_t parser_parse_level6(double* result)
{
    uint32_t err = 0;

    int op_id;
    double ans = 0;
    err = parser_parse_level7(&ans);

    op_id = parser_get_operator_id(token);
    while (op_id == POW)
    {
        parser_getToken();

        double ansl7 = 0;
        parser_parse_level7(&ansl7);
        err = parser_eval_operator(op_id, &ans, &ansl7, &ans);
        op_id = parser_get_operator_id(token);
    }

    *result = ans;
    return err;
}

/*
 * Factorial
 */
uint32_t parser_parse_level7(double* result)
{
    uint32_t err = 0;

    int op_id;
    double ans = 0;
    err = parser_parse_level8(&ans);

    op_id = parser_get_operator_id(token);
    while (op_id == FACTORIAL)
    {
        parser_getToken();
        // factorial does not need a value right from the
        // operator, so zero is filled in.
        double ansl7 = 0.0;
        err = parser_eval_operator(op_id, &ans, &ansl7, &ans);
        op_id = parser_get_operator_id(token);
    }

    *result = ans;
    return err;
}

/*
 * Unary minus
 */
uint32_t parser_parse_level8(double* result)
{
    uint32_t err = 0;
    double ans = 0;

    int op_id = parser_get_operator_id(token);
    if (op_id == MINUS)
    {
        parser_getToken();
        err = parser_parse_level9(&ans);
        ans = -ans;
    }
    else
    {
        err =parser_parse_level9(&ans);
    }

    *result = ans;
    return err;
}


/*
 * functions
 */
uint32_t parser_parse_level9(double* result)
{
    uint32_t err = 0;

    char fn_name[NAME_LEN_MAX+1];
    double ans = 0;

    if (token_type == FUNCTION)
    {
        strcpy(fn_name, token);
        parser_getToken();

        double ansl10 = 0;
        parser_parse_level10(&ansl10);
        err = parser_eval_function(fn_name, &ansl10, &ans);
    }
    else
    {
        err = parser_parse_level10(&ans);
    }

    //return ans;
    *result = ans;
    return err;
}


/*
 * parenthesized expression or value
 */
uint32_t parser_parse_level10(double* result)
{
    uint32_t err = 0;

    // check if it is a parenthesized expression
    if (token_type == DELIMETER)
    {
        if (token[0] == '(' && token[1] == '\0')
        {
            parser_getToken();

            err = parser_parse_level2(result);
            double ans = *result;

            if (token_type != DELIMETER || token[0] != ')' || token[1] || '\0')
            {
                return 3;
            }
            parser_getToken();

            //return ans;
            *result = ans;
            return err;
        }
    }

    // if not parenthesized then the expression is a value
    //*result = parser_parse_number();
    return parser_parse_number(result);
}

uint32_t parser_parse_number(double* result)
{
    uint32_t err = 0;
    double ans = 0;

    switch (token_type)
    {
        case NUMBER:
            // this is a number
            ans = strtod(token, NULL);
            parser_getToken();
            break;

        case VARIABLE:
            // this is a variable
            err = parser_eval_variable(token, &ans);
            parser_getToken();
            break;

        default:
            // syntax error or unexpected end of expression
            if (token[0] == '\0')
            {
                return 6;
            }
            else
            {
                return 7;
            }
            break;
    }

    *result = ans;
    return err;
}

/*
 * returns the id of the given operator
 * returns -1 if the operator is not recognized
 */
int parser_get_operator_id(const char op_name[])
{
    // level 2
    if (!strcmp(op_name, "&")) {return AND;}
    if (!strcmp(op_name, "|")) {return OR;}
    if (!strcmp(op_name, "<<")) {return BITSHIFTLEFT;}
    if (!strcmp(op_name, ">>")) {return BITSHIFTRIGHT;}

    // level 3
    if (!strcmp(op_name, "=")) {return EQUAL;}
    if (!strcmp(op_name, "<>")) {return UNEQUAL;}
    if (!strcmp(op_name, "<")) {return SMALLER;}
    if (!strcmp(op_name, ">")) {return LARGER;}
    if (!strcmp(op_name, "<=")) {return SMALLEREQ;}
    if (!strcmp(op_name, ">=")) {return LARGEREQ;}

    // level 4
    if (!strcmp(op_name, "+")) {return PLUS;}
    if (!strcmp(op_name, "-")) {return MINUS;}

    // level 5
    if (!strcmp(op_name, "*")) {return MULTIPLY;}
    if (!strcmp(op_name, "/")) {return DIVIDE;}
    if (!strcmp(op_name, "%")) {return MODULUS;}
    if (!strcmp(op_name, "||")) {return XOR;}

    // level 6
    if (!strcmp(op_name, "^")) {return POW;}

    // level 7
    if (!strcmp(op_name, "!")) {return FACTORIAL;}

    return -1;
}

/*
 * evaluate an operator for given values
 */
uint32_t parser_eval_operator(const int op_id, const double* lhs, const double* rhs, double* result)
{
    switch (op_id)
    {
        // level 2
        case AND:
            *result = (int)(*lhs) & (int)(*rhs);
            return 0;
        case OR:
            *result = (int)(*lhs) | (int)(*rhs);
            return 0;
        case BITSHIFTLEFT:
            *result = (int)(*lhs) << (int)(*rhs);
            return 0;
        case BITSHIFTRIGHT:
            *result = (int)(*lhs) >> (int)(*rhs);
            return 0;

        // level 3
        case EQUAL:
            *result = (fabs(*lhs - *rhs) < 0.0000001);
            return 0;
        case UNEQUAL:
            *result = (fabs(*lhs - *rhs) > 0.0000001);
            return 0;
        case SMALLER:
            *result = *lhs < *rhs;
            return 0;
        case LARGER:
            *result = *lhs > *rhs;
            return 0;
        case SMALLEREQ:
            *result = *lhs <= *rhs;
            return 0;
        case LARGEREQ:
            *result = *lhs >= *rhs;
            return 0;

        // level 4
        case PLUS:
             *result = *lhs + *rhs;
             return 0;
        case MINUS:
            *result = *lhs - *rhs;
            return 0;

        // level 5
        case MULTIPLY:
            *result = *lhs * *rhs;
            return 0;
        case DIVIDE:
            *result = *lhs / *rhs;
            return 0;
        case MODULUS:
            *result = (int)(*lhs) % (int)(*rhs); // todo: give a warning if the values are not integer?
            return 0;

        case XOR:
            *result = (int)(*lhs) ^ (int)(*rhs);
            return 0;

        // level 6
        case POW:
            *result = pow(*lhs, *rhs);
            return 0;

        // level 7
        case FACTORIAL:
            *result = factorial(*lhs);
            return 0;

    }

    return 104;
}

/*
 * evaluate a function
 */
uint32_t parser_eval_function(const char fn_name[], const double* value, double* result)
{
    // first make the function name upper case
    //char fnU[NAME_LEN_MAX+1] = ;
    //variablelist_toupper(fnU, fn_name);

    // arithmetic
    if (!strcmp(fn_name, "ABS")) {*result = abs(*value); return 0;}
    //if (!strcmp(fn_name, "EXP")) {*result =  exp(*value); return 0;}
    if (!strcmp(fn_name, "SIGN")) {*result =  sign(*value); return 0;}
    //if (!strcmp(fn_name, "SQRT")) {*result =  sqrt(*value); return 0;}
    //if (!strcmp(fn_name, "LOG")) {*result =  log(*value); return 0;}
    //if (!strcmp(fn_name, "LOG10")) {*result =  log10(*value); return 0;}

    // trigonometric
    //if (!strcmp(fn_name, "SIN")) {*result =  sin(*value); return 0;}
    //if (!strcmp(fn_name, "COS")) {*result =  cos(*value); return 0;}
    //if (!strcmp(fn_name, "TAN")) {*result =  tan(*value); return 0;}
    //if (!strcmp(fn_name, "ASIN")) {*result =  asin(*value); return 0;}
    //if (!strcmp(fn_name, "ACOS")) {*result =  acos(*value); return 0;}
    //if (!strcmp(fn_name, "ATAN")) {*result =  atan(*value); return 0;}

    // probability
    if (!strcmp(fn_name, "FACTORIAL")) {*result =  factorial(*value); return 0;}

    //
    if (!strcmp(fn_name, "ROUND")) {*result =  round(*value); return 0;}
    if (!strcmp(fn_name, "FLOOR")) {*result =  floor(*value); return 0;}

    // unknown function
    //Error(row(), col(), 102, fn_name);
    return 102;
}

/*
 * evaluate a variable
 */
uint32_t parser_eval_variable(const char var_name[], double* result)
{
    // first make the variable name uppercase
    //char varU[NAME_LEN_MAX+1];
    //variablelist_toupper(varU, var_name);

    // check for built-in variables
    if (!strcmp(var_name, "E")) {
            *result = 2.7182818284590452353602874713527;
            return 0;
    }
    if (!strcmp(var_name, "PI")) {
            *result = 3.1415926535897932384626433832795;
            return 0;
    }

    // check for user defined variables
    //double ans = 0;

    //uint8_t v = variablelist_get_value_by_name(var_name, &ans);

    /*if (v > 0)
    //if (user_var.get_value(var_name, &ans))
    {
        *result = ans;
        return 0;
    }*/

    return 103;
}

