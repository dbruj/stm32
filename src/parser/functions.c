
#include <math.h>

#include "functions.h"


/*
 * calculate factorial of value
 * for example 5! = 5*4*3*2*1 = 120
 */
double factorial(double value)
{
    double res;
    int v = (int)(value);

    if (fabs(value - (double)(v)) < 0.0000001)
    {
        //Error(-1, -1, 400, "factorial");
        return 400;
    }

    res = v;
    v--;
    while (v > 1)
    {
        res *= v;
        v--;
    }

    if (fabs(0 - res) < 0.0000001) res = 1;        // 0! is per definition 1

    return res;
}

/*
 * calculate the sign of the given value
 */
double sign(double value)
{
    if (value > 0) return 1;
    if (value < 0) return -1;
    return 0;
}
