#ifndef PARSER_H_INCLUDED
#define PARSER_H_INCLUDED

#include <stdint.h>

typedef enum _TOKENTYPE {NOTHING = -1, DELIMETER, NUMBER, VARIABLE, FUNCTION, UNKNOWN} TOKENTYPE;

enum OPERATOR_ID {AND, OR, BITSHIFTLEFT, BITSHIFTRIGHT,                 // level 2
               EQUAL, UNEQUAL, SMALLER, LARGER, SMALLEREQ, LARGEREQ,    // level 3
               PLUS, MINUS,                     // level 4
               MULTIPLY, DIVIDE, MODULUS, XOR,  // level 5
               POW,                             // level 6
               FACTORIAL};                      // level 7

void        parser_init(void);
uint32_t    parser_parse(double* result, const char new_expr[]);
uint32_t    parser_getToken(void);

uint32_t    parser_parse_level1(double* result);
uint32_t    parser_parse_level2(double* result);
uint32_t    parser_parse_level3(double* result);
uint32_t    parser_parse_level4(double* result);
uint32_t    parser_parse_level5(double* result);
uint32_t    parser_parse_level6(double* result);
uint32_t    parser_parse_level7(double* result);
uint32_t    parser_parse_level8(double* result);
uint32_t    parser_parse_level9(double* result);
uint32_t    parser_parse_level10(double* result);

int         parser_get_operator_id(const char op_name[]);
uint32_t    parser_eval_operator(const int op_id, const double* lhs, const double* rhs, double* result);
uint32_t    parser_eval_function(const char fn_name[], const double* value, double* result);
uint32_t    parser_eval_variable(const char var_name[], double* result);
uint32_t    parser_parse_number(double* result);

uint8_t isMinus(const char c);
uint8_t isWhiteSpace(const char c);
uint8_t isDelimeter(const char c);
uint8_t isNotDelimeter(const char c);
uint8_t isAlpha(const char c);
uint8_t isDigitDot(const char c);
uint8_t isDigit(const char c);

#endif // PARSER_H_INCLUDED
