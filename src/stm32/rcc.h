/*
 * rcc.h
 *
 *  Created on: 3 sie 2014
 *      Author: Dariusz
 */

#ifndef STM32_RCC_H_
#define STM32_RCC_H_

void stm32_rcc_init(void);

void stm32_flash_latency(uint32_t frequency);

uint32_t stm32_pll_start(uint32_t crystal, uint32_t frequency);

#endif /* RCC_H_ */
