/*
 * usart.c
 *
 *  Created on: 3 cze 2014
 *      Author: dbruj
 */

#include <stdint.h>
#include <stdlib.h>

#include "stm32f10x.h"

#include "config.h"
#include "gpio.h"
#include "usart.h"

#include "hdr/hdr_bitband.h"
#include "hdr/hdr_gpio.h"

void stm32_usart_init(USART_TypeDef* usart, uint32_t baudrate)
{
	//Diable USART
	usart->CR1  &= USART_CR1_UE;

	// Set GPIO pins for defined USART
	if (usart == USART1)
	{
		// Enable USART clock
		RCC->APB2ENR |= RCC_APB2ENR_USART1EN;

		// set constant frequency.
		// TODO: Get current PCLK2 frequency instead of constant.
		uint32_t frequency = FREQUENCY;

		// Set baud rate
		//usart->BRR  = (uint16_t)(frequency/baudrate);
		usart->BRR  = (uint16_t)((frequency + (baudrate>>1))/baudrate);

		// Set TX pin
		stm32_gpio_pin_cfg(USART1_OUT_GPIO, USART1_OUT_PIN, GPIO_CRx_MODE_CNF_ALT_PP_50M_value);
		// Set RX pin
		stm32_gpio_pin_cfg(USART1_IN_GPIO, USART1_IN_PIN, GPIO_CRx_MODE_CNF_IN_FLOATING_value);

		NVIC_EnableIRQ(USART1_IRQn);
	}

	if (usart == USART2)
	{
		// Enable USART clock
		RCC->APB1ENR |= RCC_APB1ENR_USART2EN;

		// set constant frequency.
		// TODO: Get current PCLK1 frequency instead of constant.
		uint32_t frequency = FREQUENCY>>1;

		// Set baud rate
		//uint16_t brr = (uint16_t)(frequency/baudrate);
		usart->BRR  = (uint16_t)((frequency + (baudrate>>1))/baudrate);

		// Set TX pin
		stm32_gpio_pin_cfg(USART2_OUT_GPIO, USART2_OUT_PIN, GPIO_CRx_MODE_CNF_ALT_PP_50M_value);
		// Set RX pin
		stm32_gpio_pin_cfg(USART2_IN_GPIO, USART2_IN_PIN, GPIO_CRx_MODE_CNF_IN_FLOATING_value);

		NVIC_EnableIRQ(USART2_IRQn);
	}

	// Set basic configuration
	usart->CR1  = 0 | USART_CR1_TE | USART_CR1_RE | USART_CR1_RXNEIE;
	usart->CR2  = 0;
	usart->CR3  = 0;
	usart->GTPR = 0;

	// Enable USART
	usart->CR1  |= USART_CR1_UE;
}

void stm32_usart_setByte(USART_TypeDef* usart, char* data)
{
	// is there any byte to send?
	while((usart->SR & USART_SR_TXE) != USART_SR_TXE);
	while((usart->SR & USART_SR_TC) != USART_SR_TC);

	// wpisz nowe
	usart->DR = (uint8_t)*data;

	// wait for end transmission
	while((usart->SR & USART_SR_TXE) != USART_SR_TXE);
	while((usart->SR & USART_SR_TC) != USART_SR_TC);
}

void stm32_usart_setString(USART_TypeDef* usart, char* string)
{
	while(*string)
		stm32_usart_setByte(usart, string++);
}

void stm32_usart_getByte(USART_TypeDef* usart, char* data)
{
	/* Wait until the data is ready to be received. */
	while ((usart->SR & USART_SR_RXNE) != USART_SR_RXNE);

	if ((usart->SR & USART_SR_RXNE) == USART_SR_RXNE)
		*data = (char)usart->DR;
}

void USART1_IRQHandler(void)
{
	if ((USART1->CR1 & USART_CR1_RXNEIE) == USART_CR1_RXNEIE)
	{
		if ((USART1->SR & USART_SR_RXNE) == USART_SR_RXNE)
		{
			// Get char data
			char buf 	=  0;
			stm32_usart_getByte(USART1, &buf);

			// ignore error bits.
			if (((USART1->SR & USART_SR_NE) != USART_SR_NE) && ((USART1->SR & USART_SR_FE) != USART_SR_FE))
			{
				// add to buffer.
				stm32_usart_setByte(USART1, &buf);
			}

			// clear IRQ flag
			USART1->SR &= (uint16_t)~USART_SR_RXNE;
		}
	}
}

void USART2_IRQHandler(void)
{
	if ((USART2->CR1 & USART_CR1_RXNEIE) == USART_CR1_RXNEIE)
	{
		if ((USART2->SR & USART_SR_RXNE) == USART_SR_RXNE)
		{
			// Get char data
			char buf 	=  0;
			stm32_usart_getByte(USART2, &buf);

			// ignore error bits.
			if (((USART2->SR & USART_SR_NE) != USART_SR_NE) && ((USART2->SR & USART_SR_FE) != USART_SR_FE))
			{
				// add to buffer.
				stm32_usart_setByte(USART2, &buf);
			}

			// clear IRQ flag
			USART2->SR &= (uint16_t)~USART_SR_RXNE;
		}
	}
}
