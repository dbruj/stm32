/*
 * spi.h
 *
 *  Created on: 3 sie 2014
 *      Author: Dariusz
 */

#ifndef STM32_SPI_H_
#define STM32_SPI_H_

uint8_t stm32_spi_transferByte(SPI_TypeDef* SPIx, uint8_t data);

size_t  stm32_spi_transfer(SPI_TypeDef* SPIx, const uint8_t *tx, uint8_t *rx, size_t length);

#endif /* STM32_SPI_H_ */
