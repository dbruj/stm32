/*
 * rtc.h
 *
 *  Created on: 4 sie 2014
 *      Author: Dariusz
 */

#ifndef RTC_H_
#define RTC_H_

#include <stdint.h>

typedef struct
{
    unsigned char second; // 0-59
    unsigned char minute; // 0-59
    unsigned char hour;   // 0-59
    unsigned char day;    // 1-31
    unsigned char month;  // 1-12
    unsigned char year;   // 0-99 (representing 2000-2099)
} stm32_rtc_datetime_t;

/**
 * Initialize RTC clock.
 */
void stm32_rtc_init(void);

/**
 * Get current RTC epoch time.
 */
uint32_t stm32_rtc_get(void);

/**
 * Set RTc epoch time.
 */
void stm32_rtc_set(uint32_t rtc_data);

/**
 * Convert datetime to epoch time.
 */
uint32_t stm32_rtc_datetimeToEpoch(stm32_rtc_datetime_t* datetime);

/**
 * Convert epoch time to datetime.
 */
void stm32_rtc_epochToDatetime(stm32_rtc_datetime_t* date_time, uint32_t epoch);

#endif /* RTC_H_ */
