/*
 * usart.h
 *
 *  Created on: 2 cze 2014
 *      Author: dbruj
 */

#ifndef USART_H_
#define USART_H_

/**
 * Init a USART
 */
void stm32_usart_init(USART_TypeDef* usart, uint32_t baudrate);

/**
 * Send byte to USART.
 */
void stm32_usart_setByte(USART_TypeDef* usart, char* data);

/**
 * Set string to USART byte by byte.
 */
void stm32_usart_setString(USART_TypeDef* usart, char* string);

/**
 * Get byte from USART.
 */
void stm32_usart_getByte(USART_TypeDef* usart, char* data);

void USART1_IRQHandler(void);

void USART2_IRQHandler(void);

void USART3_IRQHandler(void);

#endif /* USART_H_ */
