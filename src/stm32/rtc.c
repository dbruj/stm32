/*
 * rtc.c
 *
 *  Created on: 4 sie 2014
 *      Author: Dariusz
 */

#include <stdint.h>
#include <stdlib.h>

#include "stm32f10x.h"

#include "rtc.h"

void stm32_rtc_init()
{
	// Enable PWR and BKP
	RCC->APB1ENR |= RCC_APB1ENR_PWREN | RCC_APB1ENR_BKPEN;

	// Unlock backup registers.
	PWR->CR |= PWR_CR_DBP;

	// Enable LSE i and wait for ready state.
	RCC->BDCR |= RCC_BDCR_LSEON;
	while ((RCC->BDCR & RCC_BDCR_LSERDY) == 0);

	// Select RTC source
	RCC->BDCR |= RCC_BDCR_RTCSEL_LSE; 	// LSE as a source
	RCC->BDCR |= RCC_BDCR_RTCEN; 		// RTC ENABLE

	// wait for end of the write operation
	while((RTC->CRL & RTC_CRL_RTOFF) == 0);

	// enter configuration mode.
	RTC->CRL |= RTC_CRL_CNF;

	// Check if there is a needs to set counter.
	if (0)
	{
		// Get private flag

		// Set time

	}

	// set second interrupt.
	RTC->CRH |= RTC_CRH_SECIE;

	// set prescaler (1s)
	RTC->PRLH = 0;
	RTC->PRLL = 32767;

	// exit configuration mode.
	RTC->CRL &= (uint16_t)~RTC_CRL_CNF;

	// wait for end of the write operation
	while((RTC->CRL & RTC_CRL_RTOFF) == 0);

	// Lock domain backup registers
	PWR->CR &= (uint16_t)~PWR_CR_DBP;
}

uint32_t stm32_rtc_get()
{
	// Get counter
	return (uint32_t)RTC->CNTH << 16 | (uint32_t)RTC->CNTL;
}

void stm32_rtc_set(uint32_t time)
{
	// Unlock backup registers.
	PWR->CR |= PWR_CR_DBP;

	// wait for end of the write operation
	while((RTC->CRL & RTC_CRL_RTOFF) == 0);

	/* Set the CNF flag to enter in the Configuration Mode */
	RTC->CRL |= RTC_CRL_CNF;

	// Set counter
	RTC->CNTH = (uint16_t)((time >> 16) & 0xFFFF);
	RTC->CNTL = (uint16_t)(time & 0xFFFF);

	// exit configuration mode.
	RTC->CRL &= (uint16_t)~RTC_CRL_CNF;

	// wait for end of the write operation
	while((RTC->CRL & RTC_CRL_RTOFF) == 0);

	// Lock domain backup registers
	PWR->CR &= (uint16_t)~PWR_CR_DBP;
}
