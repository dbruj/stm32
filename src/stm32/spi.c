/*
 * spi.c
 *
 *  Created on: 3 sie 2014
 *      Author: Dariusz
 */

#include <stdint.h>
#include <stdlib.h>

#include "stm32f10x.h"

#include "spi.h"

#include "hdr/hdr_spi.h"

uint8_t stm32_spi_transferByte(SPI_TypeDef* SPIx, uint8_t data)
{
	/* Loop while DR register in not empty */
	while(!SPIx_SR_TXE_bb(SPIx));

	/* Send byte through the SPI2 peripheral */
	SPIx->DR = data;

	/* Wait to receive a byte */
	while(!SPIx_SR_RXNE_bb(SPIx));

	/* Return the byte read from the SPI bus */
	return (uint8_t)(SPIx->DR & 0xFF);
}

size_t  stm32_spi_transfer(SPI_TypeDef* SPIx, const uint8_t *tx, uint8_t *rx, size_t length)
{
	size_t rx_length = 0;
	uint8_t tx_byte = 0xFF;

	while(length--)
	{
		if (tx != NULL)									// should data be transfered?
			tx_byte = *tx++;							// yes - get another byte
		SPIx->DR = tx_byte;								// send data
		while (!SPIx_SR_RXNE_bb(SPIx));					// wait for transfer end
		uint8_t rx_byte = (uint8_t)(SPIx->DR & 0xFF);	// receive data
		if (rx != NULL)									// should data be received?
			*rx++ = rx_byte;							// yes - store received byte
		rx_length++;									// increment counter
	}

	return rx_length;
}

