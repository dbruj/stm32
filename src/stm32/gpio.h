
#ifndef STM32_GPIO_H_
#define STM32_GPIO_H_

void stm32_gpio_init(void);
void stm32_gpio_pin_cfg(GPIO_TypeDef *port_ptr, uint32_t pin, uint32_t mode_cnf_value);

#endif /* GPIO_H_ */
