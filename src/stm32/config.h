/** \file config.h
 */
#ifndef CONFIG_H_
#define CONFIG_H_

#define CRYSTAL								8000000ul	///< quartz crystal resonator which is connected to the chip
#define FREQUENCY							72000000ul	///< desired target frequency of the core

#define USART1_OUT_GPIO						GPIOA
#define USART1_OUT_PIN						9

#define USART1_IN_GPIO						GPIOA
#define USART1_IN_PIN						10

#define USART2_OUT_GPIO						GPIOA
#define USART2_OUT_PIN						2

#define USART2_IN_GPIO						GPIOA
#define USART2_IN_PIN						3

#define GSMON_GPIO							GPIOB
#define GSMON_PIN							13
#define GSMON_DR							GSMON_GPIO->ODR

#define GSMRST_GPIO							GPIOB
#define GSMRST_PIN							12
#define GSMRST_DR							GSMRST_GPIO->ODR

#define GSMON_bb							bitband_t m_BITBAND_PERIPH(&GSMON_DR, GSMON_PIN)
#define GSMRST_bb							bitband_t m_BITBAND_PERIPH(&GSMRST_DR, GSMRST_PIN)

#define LED1_GPIO							GPIOC
#define LED1_DR								LED1_GPIO->ODR
#define LED1_PIN							5

#define LED2_GPIO							GPIOB
#define LED2_DR								LED2_GPIO->ODR
#define LED2_PIN							0

#define LED1_bb								bitband_t m_BITBAND_PERIPH(&LED1_DR, LED1_PIN)
#define LED2_bb								bitband_t m_BITBAND_PERIPH(&LED2_DR, LED2_PIN)

#define INPUT1_GPIO							GPIOB
#define INPUT1_DR							INPUT1_GPIO->IDR
#define INPUT1_PIN							9
#define INPUT2_GPIO							GPIOB
#define INPUT2_DR							INPUT2_GPIO->IDR
#define INPUT2_PIN							8
#define INPUT3_GPIO							GPIOB
#define INPUT3_DR							INPUT3_GPIO->IDR
#define INPUT3_PIN							7
#define INPUT4_GPIO							GPIOB
#define INPUT4_DR							INPUT4_GPIO->IDR
#define INPUT4_PIN							6
#define INPUT5_GPIO							GPIOB
#define INPUT5_DR							INPUT5_GPIO->IDR
#define INPUT5_PIN							5
#define INPUT6_GPIO							GPIOD
#define INPUT6_DR							INPUT6_GPIO->IDR
#define INPUT6_PIN							2
#define INPUT7_GPIO							GPIOC
#define INPUT7_DR							INPUT7_GPIO->IDR
#define INPUT7_PIN							12
#define INPUT8_GPIO							GPIOC
#define INPUT8_DR							INPUT8_GPIO->IDR
#define INPUT8_PIN							11

#define INPUT1_bb							bitband_t m_BITBAND_PERIPH(&INPUT1_DR, INPUT1_PIN)
#define INPUT2_bb							bitband_t m_BITBAND_PERIPH(&INPUT2_DR, INPUT2_PIN)
#define INPUT3_bb							bitband_t m_BITBAND_PERIPH(&INPUT3_DR, INPUT3_PIN)
#define INPUT4_bb							bitband_t m_BITBAND_PERIPH(&INPUT4_DR, INPUT4_PIN)
#define INPUT5_bb							bitband_t m_BITBAND_PERIPH(&INPUT5_DR, INPUT5_PIN)
#define INPUT6_bb							bitband_t m_BITBAND_PERIPH(&INPUT6_DR, INPUT6_PIN)
#define INPUT7_bb							bitband_t m_BITBAND_PERIPH(&INPUT7_DR, INPUT7_PIN)
#define INPUT8_bb							bitband_t m_BITBAND_PERIPH(&INPUT8_DR, INPUT8_PIN)

#define BUTTON1_GPIO						GPIOA
#define BUTTON1_DR							BUTTON1_GPIO->IDR
#define BUTTON1_PIN							1

#define BUTTON2_GPIO						GPIOA
#define BUTTON2_DR							BUTTON2_GPIO->IDR
#define BUTTON2_PIN							0

#define BUTTON3_GPIO						GPIOC
#define BUTTON3_DR							BUTTON3_GPIO->IDR
#define BUTTON3_PIN							3

#define BUTTON1_bb							bitband_t m_BITBAND_PERIPH(&BUTTON1_DR, BUTTON1_PIN)
#define BUTTON2_bb							bitband_t m_BITBAND_PERIPH(&BUTTON2_DR, BUTTON2_PIN)
#define BUTTON3_bb							bitband_t m_BITBAND_PERIPH(&BUTTON3_DR, BUTTON3_PIN)

#define RELAY1_GPIO							GPIOA				///< GPIO port to which the LED is connected
#define RELAY1_DR							RELAY1_GPIO->ODR	///< output register for the LED
#define RELAY1_PIN							4					///< pin number of the LED

#define RELAY2_GPIO							GPIOA				///< GPIO port to which the LED is connected
#define RELAY2_DR							RELAY2_GPIO->ODR	///< output register for the LED
#define RELAY2_PIN							5					///< pin number of the LED

#define RELAY1_bb							bitband_t m_BITBAND_PERIPH(&RELAY1_DR, RELAY1_PIN)	///< bit-band "variable" to directly handle the pin
#define RELAY2_bb							bitband_t m_BITBAND_PERIPH(&RELAY2_DR, RELAY2_PIN)	///< bit-band "variable" to directly handle the pin


/******************************************************************************
* END OF FILE
******************************************************************************/
#endif /* CONFIG_H_ */
